package org.fasttrackit;

import org.fasttrackit.pages.Header;
import org.fasttrackit.pages.LoginElements;
import org.fasttrackit.pages.Pages;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.*;
import static org.fasttrackit.pages.Pages.*;
import static org.testng.Assert.*;
import static org.testng.Assert.assertNotEquals;
import static org.testng.Assert.assertTrue;


public class LoginPageTests {

    Pages pages;
    Header header;
    LoginElements loginElements;

    @BeforeTest
    public void openPage(){
        open(HOME_PAGE);
        pages = new Pages();
        header = new Header();
        loginElements = new LoginElements();

    }

    @AfterMethod
    public void cleanup(){
        refresh();
    }

    @Test
    public void verify_login_succeded(){
        loginElements.introduceUserName("standard_user");
        loginElements.introducePassword("secret_sauce");
        loginElements.loginButton().click();
        assertTrue(header.cartIcon().isDisplayed(),"Inventory page is opened");
        loginElements.logout();
    }

    @Test
    public void verify_login_with_problem_user(){
        loginElements.introduceUserName("problem_user");
        loginElements.introducePassword("secret_sauce");
        loginElements.loginButton().click();
        assertTrue(header.cartIcon().isDisplayed(),"Inventory page is opened");
        loginElements.logout();
    }

    @Test
    public void verify_performance_glitch_user(){
        loginElements.introduceUserName("performance_glitch_user");
        loginElements.introducePassword("secret_sauce");
        loginElements.loginButton().click();
        assertTrue(header.cartIcon().isDisplayed(),"Inventory page is opened");
        loginElements.logout();
    }

    @Test
    public void verify_if_the_error_message_appear(){
        loginElements.loginButton().click();
        assertEquals(loginElements.errorMsg().getText(),"Epic sadface: Username is required",
                "After clicking login, without ID, appear an error message.");
    }

    @Test
    public void verify_login_with_locked_out_user(){
        loginElements.introduceUserName("locked_out_user");
        loginElements.introducePassword("secret_sauce");
        loginElements.loginButton().click();
        assertEquals(loginElements.errorMsg().getText(),"Epic sadface: Sorry, this user has been locked out.",
                "After clicking login, appear an message that says this user has been locked out..");
    }

}
