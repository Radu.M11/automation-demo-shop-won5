package org.fasttrackit;

import org.fasttrackit.pages.*;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.*;
import static org.fasttrackit.pages.Pages.*;
import static org.testng.Assert.*;

public class ContentPageTests {

    Pages pages;
    Header header;
    LoginElements loginElements;
    CheckoutElements checkout;
    Footer footer;


    @BeforeTest
    public void openPage() {
        pages = new Pages();
        open(HOME_PAGE);
        header = new Header();
        loginElements = new LoginElements();
        checkout = new CheckoutElements();
        footer = new Footer();

        loginElements.introduceUserName("standard_user");
        loginElements.introducePassword("secret_sauce");
        loginElements.loginButton().click();
    }

    @AfterMethod
    public void cleanup() {
        refresh();
    }

    @Test
    public void verify_cart_icon() {
        header.cartIcon().click();
        assertTrue(header.cartIcon().isDisplayed(), "Cart page is opened.");
        checkout.continueShoppinhButton().click();
    }

    @Test
    public void verify_sort_mode_products() {
        header.allProducts();
        header.firstProduct();
        String firstProductTitle = header.firstProduct().getText();
        header.sortMode();
        assertTrue(header.sortMode().exists(), "Sort modes is displayed");
        header.sortMode().click();
        assertTrue(header.sortMode().isDisplayed(), "Sort type is displayed");
        header.highToLow();
        header.sortMode().click();
        header.allProductsAfterSort();
        header.firstProductAfterSort();
        String fistProductTitleAfterSort = header.firstProductAfterSort().getText();
        assertNotEquals(firstProductTitle, fistProductTitleAfterSort);
    }

    @Test
    public void verify_bar_menu() {
        header.barMenu().click();
        assertTrue(header.barMenu().isDisplayed(), "Bar menu is displayed");
    }

    @Test
    public void verify_cart_icon_updates_upon_adding_a_product_to_the_cart() {
        checkout.addToCart().click();
        assertEquals(header.productsInCart(), "1", "Cart icon updates after adding a product to the cart");

    }

    @Test
    public void verify_a_complete_checkout() {
        checkout.addToCart().click();
        header.cartIcon().click();
        assertTrue(checkout.checkout().isDisplayed(), "Cart page opened");
        checkout.checkout().click();
        assertTrue(checkout.continueButton().isDisplayed(), "Your information page is opened");
        checkout.introduceFirstName("Moldovan");
        checkout.introduceLastName("Radu");
        checkout.introduceZipCode("400607");
        checkout.continueButton().click();
        assertTrue(checkout.finishButton().isDisplayed(), "Overview page is opened, where you can see the entire order");
        checkout.finishButton().click();
        assertTrue(checkout.backToProductsButton().isDisplayed(),
                "A new page opens with a confirmation of the order and a thank you message");
        checkout.backToProductsButton().click();
    }

    @Test
    public void verify_back_to_products_button() {
        header.backpack().click();
        assertTrue(checkout.backToProductsButton().isDisplayed(), "Product page opened");
        checkout.backToProductsButton().click();
        assertTrue(header.backpack().exists(), "Page with all products exists");
    }

    @Test
    public void verify_facebook_logo() {
        footer.facebookLogo().exists();
        footer.facebookLogo().click();
        assertEquals(footer.facebookRedirectUrl(), FACEBOOK, "Facebook page is opened");

    }

    @Test
    public void verify_twitter_logo() {
        footer.twitterLogo().exists();
        footer.twitterLogo().click();
        assertEquals(footer.twitterRedirectUrl(), TWITTER, "Twitter page is opened");

    }

    @Test
    public void verify_linkedIn_logo() {
        footer.linkedInlogo().exists();
        footer.linkedInlogo().click();
        assertEquals(footer.linkedInRedirectUrl(), LINKEDIN, "LinkedIn page is opened");
    }

    @Test(dataProvider = "getProductData", dataProviderClass = DataSource.class)
    public void verify_Product_on_Page(Product product){
        assertTrue(product.isDisplayed(), "Product " + product.getProductId() + " is displayed on page");
        assertEquals(product.url(), product.getExpectedUrl(), "Product " + product.getProductId() + " page is opened");
    }

    @Test
    public void verify_logout_button() {
        header.barMenu().click();
        header.logoutButton().click();
        assertTrue(loginElements.loginButton().isDisplayed(), "After clicking logout, login page appear");
        loginElements.introduceUserName("standard_user");
        loginElements.introducePassword("secret_sauce");
        loginElements.loginButton().click();

    }
}