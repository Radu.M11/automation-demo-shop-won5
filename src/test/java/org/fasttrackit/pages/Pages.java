package org.fasttrackit.pages;

public class Pages {

    public static final String HOME_PAGE = "https://www.saucedemo.com/";
//    public static final String INVENTORY_PAGE = "https://www.saucedemo.com/inventory.html";
    public static final String FACEBOOK = "https://www.facebook.com/saucelabs";
    public static final String TWITTER = "https://twitter.com/saucelabs";
    public static final String LINKEDIN = "https://www.linkedin.com/company/sauce-labs/";
    public static final String ABOUT = "https://saucelabs.com/";



}
