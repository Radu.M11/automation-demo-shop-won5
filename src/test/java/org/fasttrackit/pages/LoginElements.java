package org.fasttrackit.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.actions;

public class LoginElements {

    private final SelenideElement userName = $("#user-name");
    private final SelenideElement userPassword = $("#password");
    private final SelenideElement loginButton = $("#login-button");
    private final SelenideElement errorMsg = $(".error-message-container h3");
    private final SelenideElement logout = $ ("#logout_sidebar_link");
    private final SelenideElement barMenu = $ ("#react-burger-menu-btn");



    public void introduceUserName(String user){
        userName.sendKeys(user);
    }

    public void introducePassword(String password){
        userPassword.sendKeys(password);
    }


    public SelenideElement errorMsg() { return errorMsg; }

    public SelenideElement logout() {
        barMenu.click();
        logout.click();
        return logout;
    }

    public SelenideElement loginButton() {
        return loginButton;
    }



}
