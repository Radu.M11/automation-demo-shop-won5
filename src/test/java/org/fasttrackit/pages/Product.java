package org.fasttrackit.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;
import static java.lang.String.format;

public class Product {

    private final SelenideElement title;



    private final String expectedUrl;

    private final String productId;

    public Product(String productId) {
        this.productId = productId;
        this.title = $(format("#item_%s_title_link", productId));
        this.expectedUrl = format("https://www.saucedemo.com/inventory-item.html?id=%s", productId);
    }

    public boolean isDisplayed() {
        return title.exists() && title.isDisplayed();
    }

    public String url() {
        return expectedUrl;

    }

    public void addToBasket() {
        SelenideElement addToBasketButton = $(".btn_small");
        addToBasketButton.click();
    }

    public String getProductId() {
        return productId;
    }

    public String getExpectedUrl() {
        return this.expectedUrl;
    }

}
