package org.fasttrackit.pages;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class Header {

    private final SelenideElement barMenuButton = $("#react-burger-menu-btn");
    private final SelenideElement cartIcon = $("#shopping_cart_container");
    private final ElementsCollection allProducts = $$(".inventory_item");
    private final SelenideElement sortMode = $(".product_sort_container");
    private final SelenideElement highToLow = $("option[value=hilo]");
    private final ElementsCollection allProductsAfterSort = $$(".inventory_item");
    private final SelenideElement firstProduct = allProducts().first();
    private final SelenideElement firsProductAfterSort = allProductsAfterSort().first();
    private final SelenideElement logoutButton = $("#logout_sidebar_link");
    private final SelenideElement backpack = $("#item_4_title_link");


    public SelenideElement barMenu() {
        return barMenuButton;
    }

    public SelenideElement cartIcon() {
        return cartIcon;
    }

    public ElementsCollection allProducts() {
        return allProducts;
    }

    public SelenideElement sortMode() {
        return sortMode;
    }

    public SelenideElement highToLow() {
        highToLow.click();
        return highToLow;
    }

    public ElementsCollection allProductsAfterSort() {
        return allProductsAfterSort;
    }

    public SelenideElement firstProduct() {
        return firstProduct;
    }

    public SelenideElement firstProductAfterSort() {
        return firsProductAfterSort;
    }

    public SelenideElement logoutButton() {
        return logoutButton;
    }



    public String productsInCart() {
        return cartIcon.getText();
    }

    public SelenideElement backpack() {
        return backpack;
    }


}
