package org.fasttrackit.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class Footer {

    private final SelenideElement facebookElement = $(".social_facebook");
    private final SelenideElement twitterElement = $(".social_twitter");
    private final SelenideElement linkedInElement = $(".social_linkedin");

    public SelenideElement facebookLogo() {return facebookElement;}

    public SelenideElement twitterLogo(){
        return twitterElement;
    }

    public SelenideElement linkedInlogo() {return linkedInElement;}

    public String facebookRedirectUrl(){
        return facebookElement.lastChild().getAttribute("href");}

    public String twitterRedirectUrl(){
        return twitterElement.lastChild().getAttribute("href");}

    public String linkedInRedirectUrl(){
        return linkedInElement.lastChild().getAttribute("href");}

}
