package org.fasttrackit.pages;

import org.testng.annotations.DataProvider;


public class DataSource {


    public static final String HOME_PAGE = "https://www.saucedemo.com/";
    public static final String INVENTORY_PAGE = "https://www.saucedemo.com/inventory.html";
    public static final String SAUCE_LABS_BIGHT_LIGHT= "0";
    public static final String SAUCE_LABS_BOLT_T_SHIRT= "1";
    public static final String SAUCE_LABS_ONESIE= "2";
    public static final String T_SHIRT_RED= "3";
    public static final String SAUCE_LABS_BACKPACK= "4";
    public static final String SAUCE_FLEECE_JACKET= "5";


    public static final Product SAUCE_LABS_BIGHT_LIGHT_PRODUCT = new Product(SAUCE_LABS_BIGHT_LIGHT);
    public static final Product SAUCE_LABS_BOLT_T_SHIRT_PRODUCT = new Product(SAUCE_LABS_BOLT_T_SHIRT);
    public static final Product SAUCE_LABS_ONESIE_PRODUCT = new Product(SAUCE_LABS_ONESIE);
    public static final Product T_SHIRT_RED_PRODUCT = new Product(T_SHIRT_RED);
    public static final Product SAUCE_LABS_BACKPACK_PRODUC = new Product(SAUCE_LABS_BACKPACK);
    public static final Product SAUCE_FLEECE_JACKET_PRODUCT = new Product(SAUCE_FLEECE_JACKET);


    @DataProvider(name = "getProductData")
    public static Object[][] getProductData() {
        Object[][] products = new Object[6][];
        products[0] = new Object[]{SAUCE_LABS_BIGHT_LIGHT_PRODUCT};
        products[1] = new Object[]{SAUCE_LABS_BOLT_T_SHIRT_PRODUCT};
        products[2] = new Object[]{SAUCE_LABS_ONESIE_PRODUCT};
        products[3] = new Object[]{T_SHIRT_RED_PRODUCT};
        products[4] = new Object[]{SAUCE_LABS_BACKPACK_PRODUC};
        products[5] = new Object[]{SAUCE_FLEECE_JACKET_PRODUCT};
        return products;
    }
}
