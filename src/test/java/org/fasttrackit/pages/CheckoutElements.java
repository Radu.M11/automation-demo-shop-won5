package org.fasttrackit.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;
import static java.lang.String.format;

public class CheckoutElements {

    private final SelenideElement addToCartButton = $(".btn_inventory");
    private final SelenideElement checkoutButton = $("#checkout");
    private final SelenideElement checkoutFirstName = $("#first-name");
    private final SelenideElement checkoutLastName = $("#last-name");
    private final SelenideElement checkoutZipCode = $("#postal-code");
    private final SelenideElement checkoutContinueButton = $("#continue");
    private final SelenideElement checkoutFinishButton = $("#finish");
    private final SelenideElement backToProductsButton = $("#back-to-products");
    private final SelenideElement continueShoppinhButton = $("#continue-shopping");

    /**
     * Page content
     */


    public SelenideElement addToCart() {
        return addToCartButton;}

    public SelenideElement checkout() {
        return checkoutButton;
    }

    public void introduceFirstName(String firstName){
        checkoutFirstName.sendKeys(firstName);}

    public void introduceLastName(String lastName){
        checkoutLastName.sendKeys(lastName);}

    public void introduceZipCode(String zipCode){
        checkoutZipCode.setValue(zipCode);}

    public SelenideElement continueButton() {
        return checkoutContinueButton;
    }

    public SelenideElement finishButton() {
        return checkoutFinishButton;
    }

    public SelenideElement backToProductsButton() {
        return backToProductsButton;
    }

    public SelenideElement continueShoppinhButton() {return continueShoppinhButton;}
}
