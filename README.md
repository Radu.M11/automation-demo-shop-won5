# Demo Shop Test Automation 

## This is the final project within the Fasttrackit test automation course, for Radu Moldovan.

### Webpage used for project: https://www.saucedemo.com/

### Description:

#### Is a demoshop site, with more functions, for exemple: login, add products to basket and remove products from basket, made a checkout, redirect to facebook or twitter, sort products, etc.
#### With that functions, I wrote automation tests.

#### I made two pages for tests, one for login tests, and another for the content.
### Tech stack used:
- Java17  
- Selenide Framework
- PageObjects Models

### Page object:
- Pages
- LoginElements
- Header
- Product
- Footer
- CheckoutElements
    

###Login tests

| Test Name                          | Execution Statuss | 
|------------------------------------|-----------------|
| Login succeded                     |Passed|
| Login with "problem user"          |Passed|
| Login with "performance user"      |Passed|
| Verify if the error message appear |Passed|
| Login with "locked user"           |Passed|


###Content Tests

| Test Name               | Execution Statuss |
|-------------------------|-------------------|
| Sort mode products      | Passed            |
| Back to products button | Passed   |
| A complete checkout     | Passed   |
| Facebook logo           | Passed   |
| Twitter logo            | Passed   |
| LinkedIn logo           | Passed   |
| Products on page        | Passed   |
| Logout button           | Passed   |
| Bar menu button         | Passed   |
